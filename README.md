 - `composer install`
 - `php bin/console doctrine:schema:update --force`
 - `php bin/console doctrine:fixtures:load`
 
You can add pictures to a post using drag and drop when editing a post