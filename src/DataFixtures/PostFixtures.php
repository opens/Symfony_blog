<?php
namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use App\Entity\Post;
use App\Entity\User;

class PostFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        // Create a user
        $user = new User();
        $user->setEmail('user@example.com');
        $user->setPassword(password_hash('password', PASSWORD_DEFAULT));
        $manager->persist($user);

        // Create posts
        for ($i = 1; $i <= 10; $i++) {
            $post = new Post();
            $post->setTitle('Post ' . $i);
            $post->setDescription('Description for post ' . $i);
            $post->setUser($user);
            $manager->persist($post);
        }

        $manager->flush();
    }
}